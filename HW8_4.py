import csv
import numpy as np
import matplotlib.pyplot as plt
file = open('SN_D_tot_V2.0.txt','r')


r = csv.reader(file, delimiter = ' ', skipinitialspace=True)
linelist = []
for row in r:
    linelist.append(row)



for i in range(len(linelist)):#this section copied from previous problem
    for j in range(7):
        linelist[i][j] = float(linelist[i][j])
ssn = []##of sun spots
ddy = []
for i in range(len(linelist)):
    ssn.append(linelist[i][4])
    ddy.append(linelist[i][3])
    

    
for i in range(len(ssn)):#removes invalid values from the number of sunspots
    if ssn[i] == -1:
        ssn[i] = np.NaN 
        
plt.plot(ddy,ssn)

