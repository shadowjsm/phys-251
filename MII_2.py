import matplotlib.pyplot as plt
y0 = 2*6371000
v0 = 0
dt = 10
y = [y0]
v = [v0]
t = [0]

def dvdt(y):
    r = 6371000
    a = -9.8*r*r/((y+r)**2)
    return a

i = 0

while y[-1]>0:
    t.append(t[-1]+dt)
    v.append(v[-1]+dt*dvdt(y[-1]))
    y.append(y[-1]+v[-1]*dt)
    
# Analytic time is incorrect.
print('Time to reach y = R_E for constant gravity:',36.1,'seconds')
'''result found by using this program by setting a = -9.8 within dvdt'''
plt.plot(t,y)
plt.grid()
    
