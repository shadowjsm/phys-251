1. 6/6
2. 5/6
3. 2/6

EC. 0

-3 File names should have an extension ".py".

Total: 10/18

I've not been taking off points for not following directions, but have here, as I warned earlier about. I've noticed that you don't double check your work enough - your Bitbucket username is wrong, the responsitory name is wrong, filenames don't have extensions and correct capitalization, etc. These all create issues on my end, but more importantly, one should in general be mindful of details, especially in code. 


