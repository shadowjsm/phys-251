 
dt = 0.07
h1 = 50
g = -9.8
ran =math.ceil( 3/dt) +1
idt = 1/dt
# (-3) This does not produce the same t array as HW4_1.py.


t1 = (np.linspace(0,3,ran)) 
y1 = (np.nan)*np.ones(ran)
idt = 1/dt

print('t    y1')



for i in range(0,ran):
    y1[i] = h1-(9.8/2.0)*t1[i]*t1[i]
    print('{0:.2f} {1:.3f}'.format(t1[i], y1[i]))

# Do not modify anything below here
 
markersize=6
if y1.size > 31:
    # Don't show dots if more than 31 values
    markersize = 0
 
plt.clf() # Clear the plot
plt.plot(t1, y1, 'r-', markersize=markersize, marker='.')
plt.grid()
plt.ylabel('Height [m]')
plt.xlabel('Time [s]')
plt.savefig('HW4_1.png', dpi=300, figsize=(8, 6))
