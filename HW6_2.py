import math
import matplotlib.pyplot as plt
import numpy as np
import scipy.integrate as scp


def dvdt(t,v):
    
    Dv =-9.8 - 0.1*v*((v**2)**(1/2))
    return Dv
t_span =(0.0,2.0)


v0 = [0]


tf = 2#final time
dt = 0.1# time step
Y  = np.zeros([math.ceil(tf/dt)])#x positions
X =  np.zeros([math.ceil(tf/dt)])#y positions
v0y = 0#initial vertical velocity
v0x = 0#initial horivontal velocity
m = 10.0#mass of object
b = 1# drag factor
vxs = np.zeros([math.ceil(tf/dt)])#table for x velocitys 
vys  = np.zeros([math.ceil(tf/dt)+1])#table for x velocitys 
vywd = np.zeros([math.ceil(tf/dt)])
ts  = np.zeros([math.ceil(tf/dt)+1])
t_eval = np.linspace(0,tf,math.ceil(tf/dt))
g  = -9.8
#the segment below defines initial values for all the array values
vxs[0] = v0x
vys[0] = v0y
vywd[0]  =v0y
X[0] = 0
Y[0] = 0
soln = scp.solve_ivp(dvdt,t_span,v0,method = 'RK23',t_eval = t_eval)
#print('t,  vy(with drag), vy(without drag), vx(with drag), vx(without drag) ')
vyex = [0]#table for exact vy values for vertival s
for i in range(1,len(ts)):
    ts[i] = i*dt #adds a number to the time steps calculated
   # X[i] = (X[i-1]+vxs[i-1]*dt)
    #Y[i] = (Y[i-1]+vys[i-1]*dt)
    nvy = (vys[i-1]+g*dt-(b/m)*dt*vys[i-1]*((vys[i-1]**2)+vxs[i-1]**2)**(1/2))#calculates the vertical velocity at the next time step
    vys[i] = nvy   #this append is to calctulate the vertical velocity at the next time step
    vyex.append(((-m*g/b)**(1/2))*math.tanh(g*i*dt/((-m*g/b)**(1/2))))
    #nvx = (vxs[i-1]-(b/m)*dt*vxs[i-1]*((vxs[i-1]**2)+vys[i-1]**2)**(1/2))
    #vxs[i] = nvx
   # vywd[i] = vywd[i-1]+g*dt
    #print(round(ts[i],2),'  ',round(vys[i],2),'  ',round(vywd[i],2),'  ',round(vxs[i],2),'  ',round(v0x,2))
    
    
    
    
    
    
    
    
    
#plt.plot(X, Y, color = 'r',label = 'overall position', linewidth = 1,marker = 'o',markersize = 3)
#the other plots track some property relative time but the oneabove tracks x and y positions relative to eachother and so shouldnt be but on the same plot as the others  
'''
plt.plot(ts, Y, color = 'y',label = 'vertical position', linewidth = 1,marker = 'o',markersize = 3)  
plt.plot(ts, X, color = 'g',label = 'horizontal position', linewidth = 1,marker = 'o',markersize = 3)  
plt.grid()
plt.ylabel('position[m]')
plt.xlabel('time [s]')
plt.legend
plt.figure()'''
#plt.plot(ts, vxs, color = 'b',label = 'horizontal speed', linewidth = 1,marker = 'o',markersize = 3)  

# Missing plots showing differences and dt=0.01.

plt.plot(ts, vys, color = 'r',label = 'forward euler', linewidth = 1,marker = 'o',markersize = 3)  
plt.xticks = ts
plt.grid()
plt.xlabel('Time [s]');
plt.ylabel('speed [m/s]');
#object1 = plt.plot(ts, vys, color = 'r')
plt.plot(ts,vyex,color= 'g', label  ='exact y velocity',linewidth = 1,marker  ='o', markersize = 3)

plt.plot(soln.t, soln.y[0], color = 'b',label = 'rk 23 y', linewidth = 1,marker = 'o',markersize = 3)  
plt.xticks = t_eval
plt.xlabel('time(s)')
plt.ylabel('speed(m/s)')

plt.legend()