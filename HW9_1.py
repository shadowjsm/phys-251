# Same as the previous program except that part of code moved to a function.
import statistics as stat
import numpy as np
import matplotlib.pyplot as plt
def sample():
        # Create a random list of Ns integers drawn from [0, ..., N-1] with replacement.
        # Each time code is executed, the list of n integers will change.
        ix = np.random.choice(N, size=Ns[Nss])
        
        #print('Indices of {0:d} values drawn from X'.format(Ns))
        #print(ix)
        
        # Extract a sample of values from X
        Xs = X[ix]
        
        xbar = np.mean(Xs)
        s2 = np.var(Xs)
        return (xbar, s2)
loops2 = []
Ne = 5000#number of experiments run
N  = 10000 # Population size
Ns = [10,100,1000,10000]    # Sample size
s2s = []
for Nss in range(len(Ns)):
    # Create a list of N values drawn from a Gaussian distribution (aka "normal
    # distribution" or "standard normal distribution")
    np.random.seed(0)
    X = np.random.randn(N)
    np.random.seed()
    
    # Force the population mean to be zero
    X = X - np.mean(X)  
    
    # Force the population std to be zero
    X = X/np.std(X)
    
    mu = np.mean(X)
    sigma2 = np.var(X)
    
    print('Population mean = {0:.6f}; Population sigma^2 = {1:.6f}'.format(mu, sigma2))
    
    # Begin modified code
    
    
    
    # End modified code
   
    s2s = []
    print('experiment # xbar s^2')
    xbars = []
    for i in range(Ne):
        (xbar, s2) = sample() 
        xbars.append(xbar)
        s2s.append(s2)
        if i %1000 == 0:
            print(i,',',xbar,',', s2)
    meanxbar = sum(xbars)/Ne
    xbarS2 = stat.stdev(xbars)
    means2 = sum(s2s)/Ne
    loops2.append(xbarS2)
    print('Ne =', Ne,'; mean(xbar) = ',meanxbar,'; mean(s2) = ',means2,'xbarstdev  =',xbarS2)



    z = xbars # Variable to plot
    num_bins = 100             # Number of bins to use in histogram
    plt.figure()	          # Start a new figure
    n, bins, patches = plt.hist(z, num_bins, facecolor='black')
    plt.xlabel('xbar') 
    plt.ylabel('# in bin')
    plt.grid()
    plt.show() 
               

plt.loglog(Ns,loops2,color='b',marker='o')
plt.xlabel('num sampled')
plt.ylabel('standard deviation')
plt.xlim(1,10**6)
print (loops2)


for i in range(3):
    print('(xbar_std for Ns = ',Ns[i+1],')/(xbar_std for Ns = ',Ns[i],') =',loops2[i+1]/loops2[i] )




