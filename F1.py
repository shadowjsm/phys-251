import matplotlib.pyplot as plt
import numpy as np
import scipy.integrate as scipy
dt = 0.1
Ti = 100
Ta = 0
final_time = 10
time_array = np.zeros(int(final_time/dt))
temps = np.zeros(int(final_time/dt))
def DTDt(t,To):
    return(dTdt(To))
    
    
def dTdt(To):
    dT = -0.1*To-0
    return(dT)
temps[0] = Ti
for i in range(int(final_time/dt-1)):
    time_array[i+1] = time_array[i]+dt
    temps[i+1] = temps[i]+dt*dTdt(temps[i])
    
plt.plot(time_array,temps,color = 'blue',label = 'forward Euler')
plt.xlabel('time(s)')
plt.ylabel('temprature(C)')

soln = scipy.solve_ivp(DTDt,[0,10],[Ti],method = 'RK45',t_eval = time_array)
plt.plot(soln.t,soln.y[0],color = 'red',label = 'runge kutta')
plt.legend()