
import numpy as np

def regress(xvalues,yvalues):
        
    xavg = sum(xvalues)/len(xvalues) 
    yavg = sum(yvalues)/len(yvalues) 
        
    xdev = np.zeros(len(xvalues))
    ydev = np.zeros(len(yvalues))
    for i in range(len(xvalues)):
        xdev[i] = xvalues[i]-xavg
        ydev[i] = yvalues,[i]-yavg
    xdev2 = xdev**2  
        
    m = sum(xdev*ydev)/sum(xdev2)
        
    b = yavg-m*xavg
    return(b,m)
    
b, m = regress(np.array([1, 2, 3]), np.array([1, 2.1, 3]))

print(b,m)