import numpy as np
N = 11#number of rectangles
low_bound = 0#lower bound of integration
up_bound = 4#upper bound of integration
width = (up_bound-low_bound)/N#the width of each rectangle
values=np.linspace(low_bound,up_bound,N+1)
area = 0

def curve(x):#this is the function you want the area under
    y = 1+x/4
    return y  


for i in range(len(values)-1):
    da  = (width*curve(values[i])+width*curve(values[i+1]))/2
    area+=da
error = 100*((6-area)/6)#for testing only
print('area is',area)
print('%error is',error,'%')
    
    

    