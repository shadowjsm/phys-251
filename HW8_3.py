import statistics as stat
linelist = []
file = open('SN_D_tot_V2.0.txt')

lines = file.readlines()#reads data file into program



for i in range(len(lines)):#converts info from data file into a nested list format
    linelist.append(lines[i].split())

for i in range(len(linelist)):
    for j in range(7):
        linelist[i][j] = float(linelist[i][j])
ssn = []##of sun spots
ddy = []
for i in range(len(linelist)):
    ssn.append(linelist[i][4])
    ddy.append(linelist[i][3])

nvv = len(ssn)#number of valid values. assume all values valid then subtract one for every invalid value
sumssn = 0
for i in range(len(ssn)):
    if ssn[i] != -1:
        sumssn += ssn[i]
    else:
        nvv-=1
avgssn = sumssn/nvv
std = stat.pstdev(ssn)
print('SSN mean = ',avgssn,', SSN std = ',std,', Number of valid values =',nvv)