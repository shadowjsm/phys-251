
'''program is a stripped down version of a more complex/general program 
so it may have some remnants of the more general case program
'''

import matplotlib.pyplot as plt
import math
import numpy as np 
tf = 3#final time
dt = 0.1# time step
Y  = np.zeros([math.ceil(tf/dt)])#x positions
v0y = 0#initial vertical velocity
ve  = np.zeros([math.ceil(tf/dt)])# exact velocity for no drag case
m = 10.0#mass of object
b = 0# drag factor
vxs = np.zeros([math.ceil(tf/dt)])#table for x velocitys 
v  = np.zeros([math.ceil(tf/dt)])#table for x velocitys 
t  = np.zeros([math.ceil(tf/dt)])
g  = -9.8
#the segment below defines initial values for all the array values

v[0] = v0y

for i in range(1,len(t)):
    t[i] = i*dt #adds a number to the time steps calculated
    ve[i] =t[i]*(-9.8) 
    nvy = (v[i-1]+g*dt-(b/m)*dt*v[i-1]*((v[i-1]**2)+vxs[i-1]**2)**(1/2))#calculates the vertical velocity at the next time step
    v[i] = nvy   #this append is to calctulate the vertical velocity at the next time step
print('t,v,ve')

for i in range(len(t)):
    print(round(t[i],2),round(v[i],2),round(ve[i],2))
   
#plt.plot(X, Y, color = 'r',label = 'overall position', linewidth = 1,marker = 'o',markersize = 3)
#the other plot track some property relative time but the oneabove tracks x and y positions relative to eachother and so shouldnt be but on the same plot as the others  
#plt.plot(t, Y, color = 'y',label = 'vertical position', linewidth = 1,marker = 'o',markersize = 3)  
#plt.plot(t, X, color = 'g',label = 'horizontal position', linewidth = 1,marker = 'o',markersize = 3)  
plt.plot(t, ve, color = 'b',label = 'exact', linewidth = 1,marker = 'o',markersize = 5)  
plt.plot(t, v, color = 'r',label = 'forward euler', linewidth = 1,marker = 'o',markersize = 3)  
plt.xticks = t
plt.grid()
plt.xlabel('Time [s]');
plt.ylabel('speed [m/s]');
#object1 = plt.plot(t, v, color = 'r')
plt.legend()
