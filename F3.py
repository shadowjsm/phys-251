#x^2 +y^2 must be less than one for the point to be inside the circle

import numpy as np
import matplotlib.pyplot as plt
circle_xs = []
circle_ys = []
non_circle_xs = []
non_circle_ys = []
for i in range(1000):
    x =1- 2*np.random.random(1)
    y =1- 2*np.random.random(1)
    if (x**2+y**2)<=1:
        circle_xs.append(x)
        circle_ys.append(y)
    else:
        non_circle_xs.append(x)
        non_circle_ys.append(y)
plt.scatter(circle_xs,circle_ys,color = 'blue',marker = 'o')
plt.scatter(non_circle_xs,non_circle_ys,color = 'red',marker = 'o')
area = 4*len(circle_xs)/1000
print('Estimated area of circle = ', round(area,3))