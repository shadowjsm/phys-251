import numpy as np
rectangles = 11#number of rectangles
low_bound = 0#lower bound of integration
up_bound = 4#upper bound of integration
width = (up_bound-low_bound)/rectangles#the width of each rectangle
values=np.linspace(low_bound,up_bound,rectangles+1)
area = 0#area starts at zero and the area od each element is added to the running total

def curve(x):#this is the function you want the area under
    y = 1+x/4
    return y  


for i in range(len(values)):
    da  = width*curve(values[i]+(width/2))
    area+=da
area-= da#the linspace creates an extra value at the end 
error = 100*((6-area)/6)#for testing only
print('area is',area)
print('%error is',error,'%')
# I don't understand why you don't follow directions ...    
