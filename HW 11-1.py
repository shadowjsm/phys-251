import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
def regress(xvalues,yvalues):
        
    xavg = sum(xvalues)/len(xvalues) 
    yavg = sum(yvalues)/len(yvalues) 
        
    xdev = np.zeros(len(xvalues))
    ydev = np.zeros(len(yvalues))
    for i in range(len(xvalues)):
        xdev[i] = xvalues[i]-xavg
        ydev[i] = yvalues[i]-yavg
    xdev2 = xdev**2  
        
    m = sum(xdev*ydev)/sum(xdev2)
        
    b = yavg-m*xavg
    return(b,m)


linelist = []

file = open('Hw_11_data.txt')

lines = file.readlines()#reads data file into program



for i in range(len(lines)):#converts info from data file into a nested list format
    linelist.append(lines[i].split(','))
for i in range(len(linelist)):
    linelist[i][0] = float(linelist[i][0])
    linelist[i][1] = float(linelist[i][1])

linelist = np.array(linelist)
linelist = np.sort(linelist,axis = 0)
linelist = linelist.transpose()
#print(linelist)


plt.plot(linelist[0],linelist[1],'ro',color = 'black',markersize = 1,label ='data' )
a = stats.linregress(linelist[0],linelist[1])
linex = [0,9]#x coordinates of regression line
liney = [a[1],9*a[0]+a[1]]
plt.plot(linex,liney,color = 'blue',label = 'regression line')
title = 'm = '+str(a[0])+'\nb = '+str(a[1])+'\nR squared = '+str(a[2])
#10 should be replaced with the number of input values
data_avg = np.zeros(10)#creates an array to store the averages of the outputs of the different input values 
for i in range(len(linelist[0])):#this method of finding an average requires the input values to be integers
    data_avg[int(linelist[0,i])]+=linelist[1,i] 
data_avg/=20#divides by the number of data points per input value to make an average rather than a sum

plt.title(title)
plt.plot([0,1,2,3,4,5,6,7,8,9],data_avg,'ro',marker = 'o',label = 'average values')
plt.legend()
plt.figure









