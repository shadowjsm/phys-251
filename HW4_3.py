
import math
import numpy as np
import matplotlib.pyplot as plt
 
dt = 0.11
h1 = 50
g = -9.8
ran =math.ceil( 3/dt) +1
idt = 1/dt



t1 = (np.linspace(0,3,ran)) 
y1 = np.ones(ran)
idt = 1/dt

# Why *= here? Leads to extra computation and y1 = h1-(9.8/2.0)*t1*t1
# is easier to understand.
y1*= h1-(9.8/2.0)*t1*t1
for i in range(len(t1)):
    print(t1[i],y1[i])
# Do not modify anything below here
 
markersize=6
if y1.size > 31:
    # Don't show dots if more than 31 values
    markersize = 0
 
plt.clf() # Clear the plot
plt.plot(t1, y1, 'r-', markersize=markersize, marker='.')
plt.grid()
plt.ylabel('Height [m]')
plt.xlabel('Time [s]')
plt.savefig('HW4_1.png', dpi=300, figsize=(8, 6))
