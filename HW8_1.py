#for some reason the program only writes to the file the second time its run the first just creates an empty file
import csv
import matplotlib.pyplot as plt
file = open('HW8_1.csv','w')
file.write('t [s], x [m], y [m]\n')
for i in range(6):
    file.write('{0:d}, {1:.1f}, {2:.1f}\n'.format(10*i,i+1,2*(i+1)))
file.close()

#second part of assn starts here
file = open('HW8_1.csv','r')



r = csv.reader(file, delimiter = ',', skipinitialspace=True)
linelist = []
for row in r:
    linelist.append(row)



for i in range(len(linelist)-1):#this section copied from previous problem
    for j in range(len(linelist[2])):
        linelist[i][j] = float(linelist[i+1][j])
        
x = []
y = []
t = []
for i in range(len(linelist)):
    t.append(linelist[i][0])
    x.append(linelist[i][1])
    y.append(linelist[i][2])
    
x.pop()
y.pop()
t.pop()

plt.plot(t,x)
plt.plot(t,y)