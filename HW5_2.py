# -*- coding: utf-8 -*-
"""
Created on Mon Sep 30 11:18:06 2019

@author: Johno
"""

import matplotlib.pyplot as plt
import math
import numpy as np 
tf = 3#final time
dt = 0.05# time step
Y  = np.zeros([math.ceil(tf/dt)])#x positions
X =  np.zeros([math.ceil(tf/dt)])#y positions
v0y = 0#initial vertical velocity
v0x = 10#initial horivontal velocity
ve  = np.zeros([math.ceil(tf/dt)])# exact velocity for no drag case
m = 10.0#mass of object
b = 1# drag factor
vxs = np.zeros([math.ceil(tf/dt)])#table for x velocitys 
v  = np.zeros([math.ceil(tf/dt)])#table for x velocitys 
t  = np.zeros([math.ceil(tf/dt)])
g  = -9.8
#the segment below defines initial values for all the array values
vxs[0] = v0x
v[0] = v0y
X[0] = 0
Y[0] = 0
ve[0] = v0y
for i in range(1,len(t)):
    
    t[i] = i*dt #adds a number to the time steps calculated
    ve[i] =v0y+t[i]*(-9.8) 
#    X[i] = (X[i-1]+vxs[i-1]*dt)
#   Y[i] = (Y[i-1]+v[i-1]*dt)
    nvy = (v[i-1]+g*dt-(b/m)*dt*v[i-1]*((v[i-1]**2)+vxs[i-1]**2)**(1/2))#calculates the vertical velocity at the next time step
    v[i] = nvy   #this append is to calctulate the vertical velocity at the next time step
  #  nvx = (vxs[i-1]-(b/m)*dt*vxs[i-1]*((vxs[i-1]**2)+v[i-1]**2)**(1/2))
 #   vxs[i] = nvx
 
print('t,v,ve')

for i in range(len(t)):
    print(round(t[i],2),round(v[i],2),round(ve[i],2))
   
 
 
# (-1) Problem statement asks for time to be plotted from 0-1.

    
#plt.plot(X, Y, color = 'r',label = 'overall position', linewidth = 1,marker = 'o',markersize = 3)
#the other plot track some property relative time but the oneabove tracks x and y positions relative to eachother and so shouldnt be but on the same plot as the others  
'''
plt.plot(t, Y, color = 'y',label = 'vertical position', linewidth = 1,marker = 'o',markersize = 3)  
plt.plot(t, X, color = 'g',label = 'horizontal position', linewidth = 1,marker = 'o',markersize = 3)  
plt.grid()
plt.ylabel('position[m]')
plt.xlabel('time [s]')
plt.legend
plt.figure()
plt.plot(t, vxs, color = 'b',label = 'horizontal speed', linewidth = 1,marker = 'o',markersize = 3)  '''
plt.plot(t, v, color = 'r',label = 'b/m = 0.1', linewidth = 1,marker = 'o',markersize = 3)  
plt.plot(t, ve, color = 'b',label = 'b/m = 0', linewidth = 1,marker = 'o',markersize = 5)
plt.xticks = t
plt.grid()
plt.xlabel('Time [s]');
plt.ylabel('speed [m/s]');
#object1 = plt.plot(t, v, color = 'r')
plt.legend()


