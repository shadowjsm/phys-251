import numpy as np
import matplotlib.pyplot as plt
linelist = []
file = open('FR1.txt')

lines = file.readlines()#reads data file into program



for i in range(len(lines)):#converts info from data file into a nested list format
    linelist.append(lines[i].split(','))
linelist = np.array(linelist)
column1 = []
column2 = []
for i in range(len(linelist)):
    column1.append(linelist[i,0])
    column2.append(linelist[i,1])
    column1[i] = int(column1[i])
    column2[i] = column2[i].rstrip()
    if column2[i]!= ' *':
        column2[i] = int(column2[i])
plt.plot(column1,column2)
count = 0
tot = 0
for i in column2:
    if i != ' *':
        count +=1
        tot +=i
avg = tot/count
print('avg =',avg )