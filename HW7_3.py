import numpy as np
rectangles = 13#number of parabolic sections
low_bound = 0#lower bound of integration
up_bound = 4#upper bound of integration
width = (up_bound-low_bound)/rectangles#the width of each parabola
values=np.linspace(low_bound,up_bound,rectangles+1)
area = 0#the area starts at zero and goes up while moving to the right in the curve

def curve(x):#this is the function you want the area under
    y = 1+x/4
    return y  


for i in range(len(values)-1):
    da  = width*(curve(values[i])+4*curve((values[i]+values[i+1])/2)+curve(values[i+1]))/6
    area+=da
#area-= da#the linspace creates an extra value at the end 
error = 100*((6-area)/6)#for testing only
print('area is',area)
print('%error is',error,'%')
    
    







