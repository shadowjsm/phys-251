import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
import math
def regress(xvalues,yvalues):
        
    xavg = sum(xvalues)/len(xvalues) 
    yavg = sum(yvalues)/len(yvalues) 
        
    xdev = np.zeros(len(xvalues))
    ydev = np.zeros(len(yvalues))
    for i in range(len(xvalues)):
        xdev[i] = xvalues[i]-xavg
        ydev[i] = yvalues[i]-yavg
    xdev2 = xdev**2  
        
    m = sum(xdev*ydev)/sum(xdev2)
        
    b = yavg-m*xavg
    return(b,m)


linelist = []

t = np.linspace(1, 3) # assume units are seconds
y = (9.8/2)*t**2 + 0.5*np.random.randn(len(t)) # assume units are meters
print(t)
linelist.append(t)
linelist.append(y)
linelist = np.array(linelist)
for i in range(len(linelist)): # len(linelist) = 2. Look at linelist - it has multiple dimensions, so you want to use output of linelist.shape
    # Also, better to avoid for loop by using np.log(linelist)
    linelist[i,0] = math.log(linelist[i,0])
    linelist[i,1] = math.log(linelist[i,1])

#print(linelist)

plt.figure
plt.plot(linelist[0],linelist[1],'ro',color = 'black',markersize = 1,label ='data' )
a = stats.linregress(linelist[0],linelist[1])

linex = [0,9]#x coordinates of regression line
liney = [a[1],9*a[0]+a[1]] # yiney should be exp(a[0])*linex**a[1] 

plt.plot(linex,liney,color = 'blue',label = 'regression line')
title = 'm = '+str(a[0])+'\nb = '+str(a[1])+'\nR squared = '+str(a[2])
plt.xlim(-0.1,3.1)
plt.ylim(-0.1,50)
plt.title(title)

plt.legend()
plt.figure()
plt.plot(t,y,'ro')

print('t = ',t)
print('log t = ',linelist[0])
print(math.e**a[1])







