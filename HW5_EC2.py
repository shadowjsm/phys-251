# This code is the same as HW5_3.py
# This code does not produce what was requested. (I don't give partial credit on EC problems.)
import matplotlib.pyplot as plt
import math
import numpy as np 
tf = 3#final time
dt = 0.1# time step
Y  = np.zeros([math.ceil(tf/dt)])#x positions
X =  np.zeros([math.ceil(tf/dt)])#y positions
v0y = 10#initial vertical velocity
v0x = 10#initial horivontal velocity
m = 10.0#mass of object
b = 1# drag factor
vxs = np.zeros([math.ceil(tf/dt)])#table for x velocitys 
vys  = np.zeros([math.ceil(tf/dt)])#table for x velocitys 
vywd = np.zeros([math.ceil(tf/dt)])
ts  = np.zeros([math.ceil(tf/dt)])
g  = -9.8
#the segment below defines initial values for all the array values
vxs[0] = v0x
vys[0] = v0y
vywd[0]  =v0y
X[0] = 0
Y[0] = 0

print('t,  vy(with drag), vy(without drag), vx(with drag), vx(without drag) ')
for i in range(1,len(ts)):
    ts[i] = i*dt #adds a number to the time steps calculated
    X[i] = (X[i-1]+vxs[i-1]*dt)
    Y[i] = (Y[i-1]+vys[i-1]*dt)
    nvy = (vys[i-1]+g*dt-(b/m)*dt*vys[i-1]*((vys[i-1]**2)+vxs[i-1]**2)**(1/2))#calculates the vertical velocity at the next time step
    vys[i] = nvy   #this append is to calctulate the vertical velocity at the next time step
    nvx = (vxs[i-1]-(b/m)*dt*vxs[i-1]*((vxs[i-1]**2)+vys[i-1]**2)**(1/2))
    vxs[i] = nvx
    vywd[i] = vywd[i-1]+g*dt
    print(round(ts[i],2),'  ',round(vys[i],2),'  ',round(vywd[i],2),'  ',round(vxs[i],2),'  ',round(v0x,2))
    
#plt.plot(X, Y, color = 'r',label = 'overall position', linewidth = 1,marker = 'o',markersize = 3)
#the other plots track some property relative time but the oneabove tracks x and y positions relative to eachother and so shouldnt be but on the same plot as the others  

plt.plot(ts, Y, color = 'y',label = 'vertical position', linewidth = 1,marker = 'o',markersize = 3)  
plt.plot(ts, X, color = 'g',label = 'horizontal position', linewidth = 1,marker = 'o',markersize = 3)  
plt.grid()
plt.ylabel('position[m]')
plt.xlabel('time [s]')
plt.legend
plt.figure()
plt.plot(ts, vxs, color = 'b',label = 'horizontal speed', linewidth = 1,marker = 'o',markersize = 3)  
plt.plot(ts, vys, color = 'r',label = 'vertical speed', linewidth = 1,marker = 'o',markersize = 3)  
plt.xticks = ts
plt.grid()
plt.xlabel('Time [s]');
plt.ylabel('speed [m/s]');
#object1 = plt.plot(ts, vys, color = 'r')
plt.legend()








