import numpy as np
import math
import matplotlib.pyplot as plt

def curve(x):#this is the function you want the area under
    y = (2*math.e**x)/math.e**4
    return y  

max_rectangles = 20#maximum number of area elements
low_bound = 0#lower bound of integration
up_bound = 4#upper bound of integration
e_area = 1.9633687
p_error = []#table for error of simpsons rule
r_error = []#middle rectangle error
t_error = []#linear error
e_area = 1.9633687
num_rectangles = np.linspace(1,max_rectangles,max_rectangles)
for rectangles in num_rectangles:

    width = (up_bound-low_bound)/int(rectangles)#the width of each area element
    values=np.linspace(low_bound,up_bound,int(rectangles+1))   
    p_area = 0
    for i in range(len(values)-1):
        
        p_da  = width*(curve(values[i])+4*curve((values[i]+values[i+1])/2)+curve(values[i+1]))/6
        p_area+=p_da
        #area-= da#the linspace creates an extra value at the end 
    error = abs(100*((e_area-p_area)/e_area))
    p_error.append(error)
    
    r_area = 0
    for i in range(len(values)-1):
        r_da  = width*curve(values[i]+(width/2))
        r_area+=r_da 
    error = abs(100*((e_area-r_area)/e_area))
    r_error.append(error)
    
    t_area = 0
    for i in range(len(values)-1):
        t_da  = (width*curve(values[i])+width*curve(values[i+1]))/2
        t_area+=t_da
    error = abs(100*((e_area-t_area)/e_area)) 
    t_error.append(error)
plt.loglog(num_rectangles,t_error,color = 'y',label = 'trapezoid_rule')
plt.loglog(num_rectangles,r_error,color = 'b', label = 'middle_rectangle_rule')
plt.loglog(num_rectangles,p_error,color = 'r',label = 'simpsons_rule')
plt.grid()
plt.legend()
# (-2). Need to label axes. I also gave code for the plot and said to use it ...

