
import math
# 1.1 problem 1 pythagorean theorem 
a = 3
b = 4
h = (a**2+b**2)**(1/2)
print('Hypotenuse length =', h) # See instructions for print formatting

# 1.2 problem 2 compute the time for an object to fall
y0 = 10 #starting height in meters
t = (2*y0/9.8)**0.5
print('time to fall ', y0, 'm = ', t, 's') # Python style - see note in solns.

# 1.3 problem 3

Ax = 1
Ay = 2
Bx = 2
By = 1

thetaA = math.atan(Ay/Ax) # calculates the angle of vector A from the x axis (Python style)
thetaA *= 180/3.1415926535 # converts thetaA into degrees


thetaB = math.atan(By/Bx)#calculates the angle of vector A from the x axis
thetaB *= 180/3.1415926535#converts thetaA into degrees
thetaDiff = thetaA-thetaB
thetaDiff = abs(thetaDiff)#makes the angle between the 2 vectors positive in case the smaller angle was listed first

diffx = Ax-Bx
diffy = Ay-By
diffm = (diffx**2+diffy**2)**0.5

print ('The angle between A and B is', thetaDiff,'degrees. The magnitude of the difference between A and B is', diffm,'.')
