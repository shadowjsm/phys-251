import numpy as np
import matplotlib.pyplot as plt
file = open('HW9_2.txt')#opens file
heights = file.readlines()#reads file into program
for i in range(len(heights)):#converts file into floating point numbers
    heights[i] = float(heights[i])
Ne = 5000
Ns = 100#this function selects num random elements from the list data
def sample(data, num):
    n = np.random.randint(0,len(data),num)
    out = []
    for i in range(len(n)):
        out.append(data[n[i]])
    return out

#everything between here and the next gap plots a histogram of all of the heights 
z = heights # Variable to plot
num_bins = 30             # Number of bins to use in histogram
plt.figure()	          # Start a new figure
n, bins, patches = plt.hist(z, num_bins, facecolor='black')
plt.xlabel('height') 
plt.ylabel('# of students')
plt.grid()
plt.show() 




xbars = []
for i in range(Ne):
    a = sample(heights,Ns)
    xbar = sum(a)/len(a)
    xbars.append(xbar)
    #if i%1000 == 0:#this section is to confirm the computer hasnt frozen for extremely large values of Ne
     #   print(i)
    
z = xbars # Variable to plot # Why define a new variable?
num_bins = 20             # Number of bins to use in histogram
plt.figure()	          # Start a new figure
n, bins, patches = plt.hist(z, num_bins, facecolor='black')
plt.xlabel('height in inches') 
plt.ylabel('# of experiments with average height x')
plt.yscale('log')
plt.grid()
plt.show() 
plt.axvline(69.8) # Put this before show to keep it on same plot.

avg = sum(xbars)/len(xbars)    
stdev = np.std(xbars)  
print('avgavg = ',avg,'stdev = ',stdev)  
    
'''    
#the probability of an experiment yielding an average height of 69.8 which is roughly 
10 standard deviations above average is small enough to be entirely absorbed in floating 
point rounding error implying that Ne>2^64 i attempted to calculate the probability using 
p = 1/(1-erf(n))whereerf is the error function and n is the number of standard deviations
 away from average
    
    
    '''