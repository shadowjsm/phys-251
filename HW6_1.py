
import matplotlib.pyplot as plt
import numpy as np
import scipy.integrate as scp
t_span = (0,2)
t_eval = np.nan*np.empty(21)
y0 = [10]
ye  = np.nan*np.empty(21)
MSE = 0
def dydt(t,y):
    return t

# Can do without a loop
for i in range( 21):
    t_eval[i] = i/10
    ye[i]  =10 +(t_eval[i]*t_eval[i])/2
    
    
soln = scp.solve_ivp(dydt, t_span, y0, method='RK23', t_eval=t_eval)
print(soln)

# This is average error not avg squared error
for j in range(21):
    
    MSE += ye[j] -soln.y[0][j]
MSE = MSE/21
print('the MSE is ',MSE)

# Plot also difference 
plt.plot(t_eval, ye, color = 'b',label = 'exact y', linewidth = 1,marker = 'o',markersize = 5)  
plt.plot(soln.t, soln.y[0], color = 'r',label = 'rk 23 y', linewidth = 1,marker = 'o',markersize = 1)  
plt.xticks = t_eval
plt.grid()
plt.legend()